package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.adaptadores.entrada.rest.conversores.ConversorDeCPFJ;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.valores.CPFJ;

import java.util.List;

public record ClienteWeb(String nomeFantasia, String razaoSocial, String cpfj, List<ContatoWeb> contatos) {
    public static ClienteWeb converterParaWeb(Cliente cliente) {
        String cpfj = cliente.getCpfj() != null ? cliente.getCpfj().formatar() : null;
        List<ContatoWeb> contatos = cliente.getContatos() != null
                ? cliente.getContatos().stream().map(ContatoWeb::converterParaWeb).toList() : null;
        return new ClienteWeb(cliente.getNomeFantasia(), cliente.getRazaoSocial(), cpfj, contatos);
    }

    public static Cliente converterParaModelo(ClienteWeb clienteWeb) {
        CPFJ cpfj = clienteWeb.cpfj() != null ? ConversorDeCPFJ.de(clienteWeb.cpfj()) : null;
        List<Contato> contatos = clienteWeb.contatos() != null
                ? clienteWeb.contatos().stream().map(ContatoWeb::converterParaModelo).toList() : null;
        return new Cliente(clienteWeb.nomeFantasia(), clienteWeb.razaoSocial(), cpfj, contatos);
    }
}
