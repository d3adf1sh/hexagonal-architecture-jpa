package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteJaCadastrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;
import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarResposta;

@Path("/cliente")
public class ControladorDeInsercaoDeCliente {
    private final CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente;

    public ControladorDeInsercaoDeCliente(CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente) {
        this.casoDeUsoDeInsercaoDeCliente = casoDeUsoDeInsercaoDeCliente;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inserir(ClienteWeb clienteWeb) {
        try {
            Cliente cliente = casoDeUsoDeInsercaoDeCliente.inserir(ClienteWeb.converterParaModelo(clienteWeb));
            return gerarResposta(Response.Status.CREATED, ClienteWeb.converterParaWeb(cliente));
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(Response.Status.BAD_REQUEST, failure.getMessage());
        } catch (ClienteJaCadastrado failure) {
            throw gerarErro(Response.Status.CONFLICT, failure.getMessage());
        }
    }
}
