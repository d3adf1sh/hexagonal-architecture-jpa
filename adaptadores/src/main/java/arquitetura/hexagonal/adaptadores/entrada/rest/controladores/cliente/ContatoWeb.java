package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.entidades.cliente.Telefone;

import java.util.List;

public record ContatoWeb(String nome, List<TelefoneWeb> telefones) {
    public static ContatoWeb converterParaWeb(Contato contato) {
        List<TelefoneWeb> telefones = contato.getTelefones() != null
                ? contato.getTelefones().stream().map(TelefoneWeb::converterParaWeb).toList() : null;
        return new ContatoWeb(contato.getNome(), telefones);
    }

    public static Contato converterParaModelo(ContatoWeb contatoWeb) {
        List<Telefone> telefones = contatoWeb.telefones() != null
                ? contatoWeb.telefones().stream().map(TelefoneWeb::converterParaModelo).toList() : null;
        return new Contato(contatoWeb.nome(), telefones);
    }
}
