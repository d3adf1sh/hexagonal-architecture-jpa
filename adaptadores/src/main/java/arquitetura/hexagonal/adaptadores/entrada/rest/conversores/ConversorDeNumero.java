package arquitetura.hexagonal.adaptadores.entrada.rest.conversores;

import arquitetura.hexagonal.modelo.valores.Numero;
import jakarta.ws.rs.core.Response;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

public final class ConversorDeNumero {
    private ConversorDeNumero() {
    }

    public static Numero de(String numero) {
        if (numero == null) {
            throw gerarErro(Response.Status.BAD_REQUEST, "Número não informado.");
        }

        try {
            return Numero.de(numero);
        } catch (IllegalArgumentException failure) {
            throw gerarErro(Response.Status.BAD_REQUEST, "Número inválido.");
        }
    }
}
