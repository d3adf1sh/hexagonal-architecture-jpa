package arquitetura.hexagonal.adaptadores.entrada.rest.resposta;

import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.core.Response;

public final class GeradorDeRespostaWeb {
    private GeradorDeRespostaWeb() {
    }

    public static ClientErrorException gerarErro(Response.Status status, String mensagem) {
        return new ClientErrorException(gerarResposta(status, mensagem));
    }

    private static Response gerarResposta(Response.Status status, String mensagem) {
        RespostaWeb respostaWeb = new RespostaWeb(status.getStatusCode(), mensagem);
        return gerarResposta(status, respostaWeb);
    }

    public static Response gerarResposta(Response.Status status, Object entidade) {
        return Response.status(status)
                .entity(entidade)
                .build();
    }
}
