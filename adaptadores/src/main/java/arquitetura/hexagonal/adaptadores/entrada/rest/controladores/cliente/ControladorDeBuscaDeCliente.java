package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.adaptadores.entrada.rest.conversores.ConversorDeCPFJ;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@Path("/cliente")
public class ControladorDeBuscaDeCliente {
    private final CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente;

    public ControladorDeBuscaDeCliente(CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente) {
        this.casoDeUsoDeBuscaDeCliente = casoDeUsoDeBuscaDeCliente;
    }

    @GET
    @Path("/{cpfj}")
    @Produces(MediaType.APPLICATION_JSON)
    public ClienteWeb buscar(@PathParam("cpfj") String cpfj) {
        try {
            Cliente cliente = casoDeUsoDeBuscaDeCliente.buscar(ConversorDeCPFJ.de(cpfj));
            return ClienteWeb.converterParaWeb(cliente);
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(Response.Status.NOT_FOUND, failure.getMessage());
        }
    }
}
