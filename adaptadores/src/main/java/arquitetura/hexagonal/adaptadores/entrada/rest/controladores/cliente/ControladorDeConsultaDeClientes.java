package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeConsultaDeClientes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@Path("/cliente")
public class ControladorDeConsultaDeClientes {
    private final CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes;

    public ControladorDeConsultaDeClientes(CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes) {
        this.casoDeUsoDeConsultaDeClientes = casoDeUsoDeConsultaDeClientes;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ClienteWeb> consultar(@QueryParam("nomeFantasia") String nomeFantasia) {
        try {
            return casoDeUsoDeConsultaDeClientes.consultar(nomeFantasia)
                    .stream()
                    .map(ClienteWeb::converterParaWeb)
                    .toList();
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(Response.Status.BAD_REQUEST, failure.getMessage());
        }
    }
}