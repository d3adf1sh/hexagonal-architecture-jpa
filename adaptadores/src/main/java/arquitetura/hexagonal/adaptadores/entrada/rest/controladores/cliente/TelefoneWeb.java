package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.adaptadores.entrada.rest.conversores.ConversorDeNumero;
import arquitetura.hexagonal.modelo.entidades.cliente.Telefone;
import arquitetura.hexagonal.modelo.valores.Numero;

public record TelefoneWeb(String descricao, String numero) {
    public static TelefoneWeb converterParaWeb(Telefone telefone) {
        String numero = telefone.getNumero() != null ? telefone.getNumero().formatar() : null;
        return new TelefoneWeb(telefone.getDescricao(), numero);
    }

    public static Telefone converterParaModelo(TelefoneWeb telefoneWeb) {
        Numero numero = telefoneWeb.numero() != null ? ConversorDeNumero.de(telefoneWeb.numero()) : null;
        return new Telefone(telefoneWeb.descricao(), numero);
    }
}
