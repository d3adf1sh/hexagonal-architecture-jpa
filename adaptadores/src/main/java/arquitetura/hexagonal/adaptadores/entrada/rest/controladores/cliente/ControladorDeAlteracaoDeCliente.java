package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@Path("/cliente")
public class ControladorDeAlteracaoDeCliente {
    private final CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente;

    public ControladorDeAlteracaoDeCliente(CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente) {
        this.casoDeUsoDeAlteracaoDeCliente = casoDeUsoDeAlteracaoDeCliente;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ClienteWeb alterar(ClienteWeb clienteWeb) {
        try {
            Cliente cliente = casoDeUsoDeAlteracaoDeCliente.alterar(ClienteWeb.converterParaModelo(clienteWeb));
            return ClienteWeb.converterParaWeb(cliente);
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(Response.Status.BAD_REQUEST, failure.getMessage());
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(Response.Status.NOT_FOUND, failure.getMessage());
        }
    }
}
