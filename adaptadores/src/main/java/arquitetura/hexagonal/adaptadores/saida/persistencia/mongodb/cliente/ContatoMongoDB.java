package arquitetura.hexagonal.adaptadores.saida.persistencia.mongodb.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.entidades.cliente.Telefone;
import org.bson.Document;

import java.util.List;

public final class ContatoMongoDB {
    public static Document converterDocumento(Contato contato) {
        Document documentoDeContato = new Document();
        documentoDeContato.append("nome", contato.getNome());
        if (contato.getTelefones() != null) {
            documentoDeContato.append("telefones",
                    contato.getTelefones().stream().map(TelefoneMongoDB::converterParaDocumento).toList());
        }

        return documentoDeContato;
    }

    public static Contato converterParaModelo(Document documentoDeContato) {
        List<Telefone> telefones = documentoDeContato.containsKey("telefones")
                ? documentoDeContato.getList("telefones", Document.class).stream().map(TelefoneMongoDB::converterParaModelo).toList()
                : null;
        return new Contato(documentoDeContato.getString("nome"), telefones);
    }
}