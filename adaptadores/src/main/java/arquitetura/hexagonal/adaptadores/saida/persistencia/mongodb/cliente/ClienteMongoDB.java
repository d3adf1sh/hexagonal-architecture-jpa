package arquitetura.hexagonal.adaptadores.saida.persistencia.mongodb.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.valores.CPFJ;
import org.bson.Document;

import java.util.List;

public final class ClienteMongoDB {
    public static Document converterParaDocumento(Cliente cliente) {
        Document documentoDeCliente = new Document();
        documentoDeCliente.append("nomeFantasia", cliente.getNomeFantasia());
        documentoDeCliente.append("razaoSocial", cliente.getRazaoSocial());
        if (cliente.getCpfj() != null) {
            documentoDeCliente.append("cpfj", cliente.getCpfj().valor());
        }

        if (cliente.getContatos() != null) {
            documentoDeCliente.append("contatos",
                    cliente.getContatos().stream().map(ContatoMongoDB::converterDocumento).toList());
        }

        return documentoDeCliente;
    }

    public static Cliente converterParaModelo(Document documentoDeCliente) {
        CPFJ cpfj = documentoDeCliente.containsKey("cpfj") ? new CPFJ(documentoDeCliente.getString("cpfj")) : null;
        List<Contato> contatos = documentoDeCliente.containsKey("contatos")
                ? documentoDeCliente.getList("contatos", Document.class).stream().map(ContatoMongoDB::converterParaModelo).toList()
                : null;
        return new Cliente(documentoDeCliente.getString("nomeFantasia"), documentoDeCliente.getString("razaoSocial"),
                cpfj, contatos);
    }
}