package arquitetura.hexagonal.adaptadores.saida.persistencia.mongodb.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Telefone;
import arquitetura.hexagonal.modelo.valores.Numero;
import org.bson.Document;

public final class TelefoneMongoDB {
    public static Document converterParaDocumento(Telefone telefone) {
        Document documentoDeTelefone = new Document();
        documentoDeTelefone.append("descricao", telefone.getDescricao());
        if (telefone.getNumero() != null) {
            documentoDeTelefone.append("numero", telefone.getNumero().valor());
        }

        return documentoDeTelefone;
    }

    public static Telefone converterParaModelo(Document documentoDeTelefone) {
        Numero numero = documentoDeTelefone.containsKey("numero")
                ? new Numero(documentoDeTelefone.getString("numero")) : null;
        return new Telefone(documentoDeTelefone.getString("descricao"), numero);
    }
}