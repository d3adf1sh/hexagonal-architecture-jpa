package arquitetura.hexagonal.adaptadores.saida.persistencia.jpa.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.valores.CPFJ;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Cliente")
public class ClienteJPA {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCliente;
    private String nomeFantasia;
    private String razaoSocial;
    @Column(unique = true)
    private String cpfj;
    @OneToMany(mappedBy = "cliente", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ContatoJPA> contatos;
    @Column(updatable = false)
    private LocalDateTime dataDeInsercao;
    @Column(insertable = false)
    private LocalDateTime dataDeAlteracao;

    public static ClienteJPA converterParaJPA(Cliente cliente) {
        ClienteJPA clienteJPA = new ClienteJPA();
        clienteJPA.setNomeFantasia(cliente.getNomeFantasia());
        clienteJPA.setRazaoSocial(cliente.getRazaoSocial());
        clienteJPA.setCpfj(cliente.getCpfj() != null ? cliente.getCpfj().valor() : null);
        if (cliente.getContatos() != null) {
            clienteJPA.setContatos(
                    cliente.getContatos().stream().map(contato ->
                            ContatoJPA.converterParaJPA(clienteJPA, contato)).toList());
        }

        return clienteJPA;
    }

    public static Cliente converterParaModelo(ClienteJPA clienteJPA) {
        CPFJ cpfj = clienteJPA.getCpfj() != null ? new CPFJ(clienteJPA.getCpfj()) : null;
        List<Contato> contatos = clienteJPA.getContatos() != null
                ? clienteJPA.getContatos().stream().map(ContatoJPA::converterParaModelo).toList() : null;
        return new Cliente(clienteJPA.getNomeFantasia(), clienteJPA.getRazaoSocial(), cpfj, contatos);
    }
}