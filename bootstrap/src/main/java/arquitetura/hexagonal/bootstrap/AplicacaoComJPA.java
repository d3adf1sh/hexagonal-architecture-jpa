package arquitetura.hexagonal.bootstrap;

import arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente.ControladorDeAlteracaoDeCliente;
import arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente.ControladorDeBuscaDeCliente;
import arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente.ControladorDeConsultaDeClientes;
import arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente.ControladorDeExclusaoDeCliente;
import arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente.ControladorDeInsercaoDeCliente;
import arquitetura.hexagonal.adaptadores.saida.persistencia.jpa.ProvedorDeFabricasDeGerenciadoresDeEntidades;
import arquitetura.hexagonal.adaptadores.saida.persistencia.jpa.cliente.RepositorioDeClientesJPA;
import arquitetura.hexagonal.adaptadores.saida.persistencia.mongodb.cliente.RepositorioDeClientesMongoDB;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeConsultaDeClientes;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeConsultaDeClientes;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeExclusaoDeCliente;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeInsercaoDeCliente;
import jakarta.persistence.EntityManagerFactory;
import jakarta.ws.rs.core.Application;

import java.util.Map;
import java.util.Set;

public final class AplicacaoComJPA extends Application {
    private RepositorioDeClientes repositorioDeClientes;

    @Override
    public Set<Object> getSingletons() {
        iniciarAdaptadoresDePersistencia();
        return Set.of(criarControladorDeInsersaoDeCliente(),
                criarControladorDeAlteracaoDeCliente(),
                criarControladorDeExclusaoDeCliente(),
                criarControladorDeBuscaDeCliente(),
                criarControladorDeConsultaDeClientes());
    }

    private void iniciarAdaptadoresDePersistencia() {
        String persistencia = lerPropriedade("persistencia", "");
        switch (persistencia) {
            case "mysql" -> iniciarAdaptadoresParaMySQL();
            case "postgres" -> iniciarAdaptadoresParaPostgres();
            case "mongodb" -> iniciarAdaptadoresParaMongoDB();
            default -> throw new IllegalArgumentException("Persistência \"%s\" inválida.".formatted(persistencia));
        }
    }

    private void iniciarAdaptadoresParaMySQL() {
        iniciarAdaptadoresParaJPA("com.mysql.jdbc.Driver",
                "jdbc:mysql://%s:%d/%s".formatted(lerPropriedade("persistencia.mysql.servidor", "localhost"),
                        Integer.parseInt(lerPropriedade("persistencia.mysql.porta", "3306")),
                        lerPropriedade("persistencia.mysql.bancoDeDados")),
                lerPropriedade("persistencia.mysql.usuario"),
                lerPropriedade("persistencia.mysql.senha"));
    }

    private void iniciarAdaptadoresParaPostgres() {
        iniciarAdaptadoresParaJPA("org.postgresql.Driver",
                "jdbc:postgresql://%s:%d/%s".formatted(lerPropriedade("persistencia.postgres.servidor", "localhost"),
                        Integer.parseInt(lerPropriedade("persistencia.postgres.porta", "5432")),
                        lerPropriedade("persistencia.postgres.bancoDeDados")),
                lerPropriedade("persistencia.postgres.usuario"),
                lerPropriedade("persistencia.postgres.senha"));
    }

    private void iniciarAdaptadoresParaJPA(String driver, String url, String usuario, String senha) {
        EntityManagerFactory fabricaDeGerenciadoresDeEntidades =
                ProvedorDeFabricasDeGerenciadoresDeEntidades.criarFabricaDeGerenciadoresDeEntidades(
                        Map.of("jakarta.persistence.jdbc.driver", driver,
                                "jakarta.persistence.jdbc.url", url,
                                "jakarta.persistence.jdbc.user", usuario,
                                "jakarta.persistence.jdbc.password", senha,
                                "hibernate.show_sql", "true",
                                "hibernate.format_sql", "true",
                                "hibernate.hbm2ddl.auto", "update"));
        repositorioDeClientes = new RepositorioDeClientesJPA(fabricaDeGerenciadoresDeEntidades);
    }

    private void iniciarAdaptadoresParaMongoDB() {
        String nomeDoBancoDeDados = lerPropriedade("persistencia.mongodb.bancoDeDados");
        String url = "mongodb://%s:%s@%s:%d/?authSource=%s".formatted(lerPropriedade("persistencia.mongodb.usuario"),
                lerPropriedade("persistencia.mongodb.senha"),
                lerPropriedade("persistencia.mongodb.servidor", "localhost"),
                Integer.parseInt(lerPropriedade("persistencia.mongodb.porta", "27017")),
                nomeDoBancoDeDados);
        repositorioDeClientes = new RepositorioDeClientesMongoDB(url, nomeDoBancoDeDados);
    }

    private ControladorDeInsercaoDeCliente criarControladorDeInsersaoDeCliente() {
        CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente = new ServicoDeInsercaoDeCliente(
                repositorioDeClientes);
        return new ControladorDeInsercaoDeCliente(casoDeUsoDeInsercaoDeCliente);
    }

    private ControladorDeAlteracaoDeCliente criarControladorDeAlteracaoDeCliente() {
        CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente = new ServicoDeAlteracaoDeCliente(
                repositorioDeClientes);
        return new ControladorDeAlteracaoDeCliente(casoDeUsoDeAlteracaoDeCliente);
    }

    private ControladorDeExclusaoDeCliente criarControladorDeExclusaoDeCliente() {
        CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente = new ServicoDeExclusaoDeCliente(
                repositorioDeClientes);
        return new ControladorDeExclusaoDeCliente(casoDeUsoDeExclusaoDeCliente);
    }

    private ControladorDeBuscaDeCliente criarControladorDeBuscaDeCliente() {
        CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente = new ServicoDeBuscaDeCliente(repositorioDeClientes);
        return new ControladorDeBuscaDeCliente(casoDeUsoDeBuscaDeCliente);
    }

    private ControladorDeConsultaDeClientes criarControladorDeConsultaDeClientes() {
        CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes = new ServicoDeConsultaDeClientes(
                repositorioDeClientes);
        return new ControladorDeConsultaDeClientes(casoDeUsoDeConsultaDeClientes);
    }

    private String lerPropriedade(String nome) {
        return lerPropriedade(nome, null);
    }

    private String lerPropriedade(String nome, String padrao) {
        String valor = System.getProperty(nome);
        if (valor == null || valor.isEmpty()) {
            if (padrao == null) {
                throw new IllegalArgumentException("Propriedade \"%s\" não configurada.".formatted(nome));
            }

            valor = padrao;
        }

        return valor;
    }
}