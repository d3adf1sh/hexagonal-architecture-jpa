package arquitetura.hexagonal.modelo.entidades.cliente;

import arquitetura.hexagonal.modelo.valores.Numero;
import lombok.Getter;

import java.util.Objects;

@Getter
public final class Telefone {
    private final String descricao;
    private final Numero numero;

    public Telefone(String descricao, Numero numero) {
        Objects.requireNonNull(descricao, "O argumento \"descricao\" não pode ser nulo.");
        if (descricao.isEmpty()) {
            throw new IllegalArgumentException("O argumento \"descricao\" não pode ser vazio.");
        }

        Objects.requireNonNull(numero, "O argumento \"numero\" não pode ser nulo.");
        this.descricao = descricao;
        this.numero = numero;
    }
}
