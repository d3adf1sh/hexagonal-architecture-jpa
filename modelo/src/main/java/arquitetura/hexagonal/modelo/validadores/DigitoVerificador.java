package arquitetura.hexagonal.modelo.validadores;

import java.util.Objects;

public final class DigitoVerificador {
    private final int[] peso;

    public DigitoVerificador(int[] peso) {
        Objects.requireNonNull(peso, "O argumento \"peso\" não pode ser nulo.");
        if (peso.length == 0) {
            throw new IllegalArgumentException("O argumento \"peso\" deve ter ao menos um dígito.");
        }

        this.peso = peso;
    }

    public boolean valido(String numero) {
        Objects.requireNonNull(numero, "O argumento \"numero\" não pode ser nulo.");
        int caracteres = peso.length + 1;
        if (numero.length() != caracteres) {
            throw new IllegalArgumentException("O argumento \"numero\" deve ter %d caracteres.".formatted(caracteres));
        }

        if (!numero.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("O argumento \"numero\" deve ser composto apenas por dígitos.");
        }

        String numeroBase = numero.substring(0, numero.length() - 2);
        int primeiroDV = calcular(numeroBase);
        int segundoDV = calcular(numeroBase + primeiroDV);
        return numero.equals(numeroBase + primeiroDV + segundoDV);
    }

    private int calcular(String numeroBase) {
        int i = numeroBase.length() - 1;
        int soma = 0;
        while (i >= 0) {
            int digit = Integer.parseInt(numeroBase.substring(i, i + 1));
            soma += digit * peso[peso.length - numeroBase.length() + i];
            i--;
        }

        soma = 11 - soma % 11;
        return soma > 9 ? 0 : soma;
    }
}
