package arquitetura.hexagonal.aplicacao.servicos.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;

import java.util.Objects;

public final class ServicoDeAlteracaoDeCliente implements CasoDeUsoDeAlteracaoDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeAlteracaoDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public Cliente alterar(Cliente cliente) throws ClienteNaoEncontrado {
        Objects.requireNonNull(cliente, "O argumento \"cliente\" não pode ser nulo.");
        return repositorioDeClientes.alterar(cliente)
                .orElseThrow(() -> new ClienteNaoEncontrado(
                        "Cliente %s não encontrado.".formatted(cliente.getCpfj().formatar())));
    }
}
