package arquitetura.hexagonal.aplicacao.servicos.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.valores.CPFJ;

import java.util.Objects;

public final class ServicoDeExclusaoDeCliente implements CasoDeUsoDeExclusaoDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeExclusaoDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public Cliente excluir(CPFJ cpfj) throws ClienteNaoEncontrado {
        Objects.requireNonNull(cpfj, "O argumento \"cpfj\" não pode ser nulo.");
        return repositorioDeClientes.excluir(cpfj)
                .orElseThrow(() -> new ClienteNaoEncontrado("Cliente %s não encontrado.".formatted(cpfj.formatar())));
    }
}
