package arquitetura.hexagonal.aplicacao.portas.saida;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.valores.CPFJ;

import java.util.List;
import java.util.Optional;

public interface RepositorioDeClientes {
    Cliente inserir(Cliente cliente);

    Optional<Cliente> alterar(Cliente cliente);

    Optional<Cliente> excluir(CPFJ cpfj);

    Optional<Cliente> buscar(CPFJ cpfj);

    List<Cliente> consultar(String nomeFantasia);
}
